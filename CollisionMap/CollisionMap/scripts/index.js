﻿(function () {
    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {
    };

    var slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'padding': 256,
        'tolerance': 70
    });
    $('.filters-toggle').click(function () {
        slideout.toggle();
    });

    function loadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(51.507, -0.128),
            mapTypeControlOptions: { mapTypeIds: [google.maps.MapTypeId.ROADMAP] },
            minZoom: 9,
            maxZoom: 17
        }
        var map = new google.maps.Map(document.getElementById("gMap"), mapOptions);
        return map;
    };

    function loadMarkerClusterer(data) {
        var clusterStyles = [
            {
                url: 'Collision_Image-01.png',
                height: 25,
                width: 25,
                textColor: 'white'
            },
            {
                url: 'Collision_Image-02.png',
                height: 35,
                width: 35,
                textColor: 'white'
            },
            {
                url: 'Collision_Image-03.png',
                height: 50,
                width: 50,
                textColor: 'white'
            }
        ];

        var mcOptions = {
            gridSize: 100,
            styles: clusterStyles,
            maxZoom: 16,
            ignoreHidden: true
        }

        var scaledFatalIcon = new google.maps.MarkerImage('Fatal.png', null, null, null, new google.maps.Size(20, 20));
        var scaledSeriousIcon = new google.maps.MarkerImage('Serious.png', null, null, null, new google.maps.Size(20, 20));
        if (collisionMap.getZoom() > 12) {
            for (var i = 0; i < data.length; i++) {
                var latLng = new google.maps.LatLng(data[i].lat, data[i].lon);
                var marker = {};
                if (data[i].severity === "Serious") {
                    marker = new google.maps.Marker({ 'position': latLng, "icon": scaledSeriousIcon, map: collisionMap });
                } else if (data[i].severity === "Fatal") {
                    marker = new google.maps.Marker({ 'position': latLng, "icon": scaledFatalIcon, map: collisionMap });
                }

                var location = '<div><img width=25 height=25 src="' + data[i].severity + '.png"/></div><div style="margin-top:10px;">' + data[i].location + '</div>';
                var casualties = '<table style="text-align:center;"><tbody><tr><td><strong>Ageband</strong></td><td><strong>Mode</strong></td><td><strong>Severity</strong></td></tr>';
                for (var x = 0; x < data[i].casualties.length; x++) {
                    var mode = data[i].casualties[x].mode;
                    if (mode === 'PedalCycle') {
                        mode = 'Cyclist';
                    }
                    if (mode !== 'PedalCycle' && mode !== 'Pedestrian') {
                        mode = 'Motor vehicle';
                    }
                    casualties += '<tr><td>' + data[i].casualties[x].ageBand + '</td>';
                    casualties += '<td>' + mode + '</td>';
                    casualties += '<td>' + data[i].casualties[x].severity + '</td></tr>';
                }
                casualties += '</tbody></table>';
                bindInfoWindow(marker, collisionMap, '<div>' + location + casualties + '</div>');
                collisionMarkers.push(marker);
            }
            markerCluster = new MarkerClusterer(collisionMap, collisionMarkers, mcOptions);

            google.maps.event.addListener(collisionMap, 'dragstart', function() {
                markerCluster.zoomOnClick_ = false;
            });

            google.maps.event.addListener(collisionMap, 'dragend', function() {
                setTimeout(function() {
                    markerCluster.zoomOnClick_ = true;
                }, 1);
            });
        }
    };

    function setMapOnAll(visibility) {
        if (heatMap && heatMap.getMap()) {
            for (var i = 0; i < collisionMarkers.length; i++) {
                collisionMarkers[i].setVisible(false);
            }
            return;
        }

        var mapBounds = collisionMap.getBounds();
        for (var i = 0; i < collisionMarkers.length; i++) {
            var latlng = collisionMarkers[i].getPosition();
            if (mapBounds.contains(latlng)) {
                collisionMarkers[i].setVisible(visibility);
            } else {
                collisionMarkers[i].setVisible(!visibility);
            }
        }
    };

    function showMarkers() {
        setMapOnAll(true);

        if (markerCluster.minClusterSize_ != 2) {
            markerCluster.minClusterSize_ = 2;
        }
    };


    function hideMarkers() {
        setMapOnAll(false);
        if (markerCluster && markerCluster.minClusterSize_ != maxClusterSize) {
            markerCluster.minClusterSize_ = maxClusterSize;
        }
    };

    function bindInfoWindow(marker, map, html) {
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(html);
            infowindow.open(map, marker);
        });
        google.maps.event.addListener(marker, 'bounds_changed', function () {
            this.close();
        });
    }

    function clearMarkers() {
        for (var i = 0; i < collisionMarkers.length; i++) {
            collisionMarkers[i].setMap(null);
        }
        collisionMarkers = [];
        if (markerCluster) {
            markerCluster.clearMarkers();
        }
    };

    function initSearchBox(collisionMap) {
        var input = document.getElementById('search-box');
        var searchBox = new google.maps.places.SearchBox(input);
        collisionMap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var markers = [];
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length === 0 ||
            (places[0].geometry.location.lat() > 51.6723432 || places[0].geometry.location.lat() < 51.3849401 || places[0].geometry.location.lng()
                < -0.351468299999965 || places[0].geometry.location.lng() > 0.148271000000022)) {
                $('#search-box').val('London locations only...');
                return;
            }
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                markers.push(new google.maps.Marker({
                    map: collisionMap,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            collisionMap.fitBounds(bounds);
        });
    }

    var accidentData;
    var collisionMarkers = [];
    var infowindow = new google.maps.InfoWindow();
    var markerCluster;
    var heatMap;
    var points = [];
    var maxClusterSize = 50000;
    var minClusterSize = 2;

    var collisionMap = loadMap();
    collisionMap.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(51.6723432, -0.351468299999965), new google.maps.LatLng(51.3849401, 0.148271000000022)));
    initSearchBox(collisionMap);
    $.getJSON('data/2014.json', function (data) {
        accidentData = data;
        loadHeatMap();
    });

    $('#range-select').change(function () {
        $('.update-map').click();
    });

    google.maps.event.addListener(collisionMap, 'zoom_changed', renderNewState);

    function renderNewState(updateState) {
        if (updateState) {
            loadMarkerClusterer(accidentData);
            loadHeatMap(true);
        }

        if (collisionMap.getZoom() > 12) {
            heatMap.setMap(null);

            if (!markerCluster && accidentData.length) {
                loadMarkerClusterer(accidentData);
            }
            showMarkers();
        } else {
            heatMap.setMap(collisionMap);
            hideMarkers();
        }
    };


    google.maps.event.addListener(collisionMap, 'drag', function () {
        slideout.destroy();
    });

    google.maps.event.addListener(collisionMap, 'dragend', function () {
        slideout = new Slideout({
            'panel': document.getElementById('panel'),
            'menu': document.getElementById('menu'),
            'padding': 256,
            'tolerance': 70
        });
        var currentLat = collisionMap.getCenter().lat();
        var currentLng = collisionMap.getCenter().lng();
        if (currentLat > 52.0863 || currentLat < 50.8285 || currentLng < -1.3129 || currentLng > 1.2085) {
            collisionMap.fitBounds(new google.maps.LatLngBounds(new google.maps.LatLng(51.6723432, -0.351468299999965), new google.maps.LatLng(51.3849401, 0.148271000000022)));
        }
    });

    function loadHeatMap(updateState) {
        points = $.map(accidentData, function (val, i) {
            return new google.maps.LatLng(val.lat, val.lon);
        });

        if (updateState) {
            heatMap.setData(points);
        }
        else {
            heatMap = new google.maps.visualization.HeatmapLayer({
                data: points,
                map: collisionMap
            });
        }
    };

    $('.filter-section.selections div').click(function () {
        $(this).toggleClass('filter-selected');
    });

    $('.update-map').click(function () {
        if ($('.update-map').hasClass('low-opacity')) {
            return false;
        }
        var file = 'data/' + $('#range-select').val() + '.json';
        $.getJSON(file, function (data) {
            var includeFatal = $('#Fatal').hasClass('filter-selected');
            var includeSerious = $('#Serious').hasClass('filter-selected');
            var includePedestrian = $('#Pedestrian').hasClass('filter-selected');
            var includeCyclist = $('#Cyclist').hasClass('filter-selected');
            var includeVehicleOccupant = $('#VehicleOccupant').hasClass('filter-selected');
            var includeAdult = $('#Adult').hasClass('filter-selected');
            var includeChild = $('#Child').hasClass('filter-selected');
            var includeUnknown = $('#Unknown').hasClass('filter-selected');

            //Severity
            if (includeFatal && !includeSerious) {
                data = $.grep(data, function (n, i) {
                    return n.severity === 'Fatal';
                });
            }
            if (!includeFatal && includeSerious) {
                data = $.grep(data, function (n, i) {
                    return n.severity === 'Serious';
                });
            }

            //Casualties
            if (includeCyclist && !includeVehicleOccupant && !includePedestrian) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].mode === 'PedalCycle') return true;
                    }
                });
            }
            if (includeCyclist && includeVehicleOccupant && !includePedestrian) {
                data = $.grep(data, function (n, i) {
                    var include = false;
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].mode !== 'Pedestrian') {
                            include = true;
                        }
                    }
                    return include;
                });
            }
            if (includeCyclist && !includeVehicleOccupant && includePedestrian) {
                data = $.grep(data, function (n, i) {
                    var include = false;
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].mode === 'PedalCycle' || n.casualties[x].mode === 'Pedestrian') {
                            include = true;
                        }
                    }
                    return include;
                });
            }
            if (!includeCyclist && !includeVehicleOccupant && includePedestrian) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].mode === 'Pedestrian') return true;
                    }
                });
            }
            if (!includeCyclist && includeVehicleOccupant && includePedestrian) {
                data = $.grep(data, function (n, i) {
                    var include = false;
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].mode !== 'PedalCycle') {
                            include = true;
                        }
                    }
                    return include;
                });
            }
            if (!includeCyclist && includeVehicleOccupant && !includePedestrian) {
                data = $.grep(data, function (n, i) {
                    var include = false;
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].mode !== 'PedalCycle' && n.casualties[x].mode !== 'Pedestrian') {
                            include = true;
                        }
                    }
                    return include;
                });
            }

            //Age bands
            if (includeAdult && !includeChild && !includeUnknown) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].ageBand === 'Adult') return true;
                    }
                });
            }
            if (includeAdult && includeChild && !includeUnknown) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].ageBand === 'Adult' || casualties[x].ageBand === 'Child') return true;
                    }
                });
            }
            if (includeAdult && !includeChild && includeUnknown) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].ageBand === 'Adult' || casualties[x].ageBand === 'Unknown') return true;
                    }
                });
            }
            if (!includeAdult && !includeChild && includeUnknown) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].ageBand === 'Unknown') return true;
                    }
                });
            }
            if (!includeAdult && includeChild && includeUnknown) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].ageBand === 'Child' || casualties[x].ageBand === 'Unknown') return true;
                    }
                });
            }
            if (!includeAdult && includeChild && !includeUnknown) {
                data = $.grep(data, function (n, i) {
                    var casualties = n.casualties;
                    for (var x = 0; x < casualties.length; x++) {
                        if (casualties[x].ageBand === 'Child') return true;
                    }
                });
            }
            accidentData = data;
            clearMarkers();
            renderNewState(true);
        });
        $('.filters-toggle').click();
    });

    $('.filter-section.selections div').click(function () {
        var includeFatal = $('#Fatal').hasClass('filter-selected');
        var includeSerious = $('#Serious').hasClass('filter-selected');
        var includePedestrian = $('#Pedestrian').hasClass('filter-selected');
        var includeCyclist = $('#Cyclist').hasClass('filter-selected');
        var includeVehicleOccupant = $('#VehicleOccupant').hasClass('filter-selected');
        var includeAdult = $('#Adult').hasClass('filter-selected');
        var includeChild = $('#Child').hasClass('filter-selected');
        var includeUnknown = $('#Unknown').hasClass('filter-selected');

        if ((!includeAdult && !includeChild && !includeUnknown) || (!includePedestrian && !includeCyclist && !includeVehicleOccupant) || (!includeFatal && !includeSerious)) {
            $('.update-map').addClass('low-opacity');
        } else {
            $('.update-map').removeClass('low-opacity');
        }
    });

    $('.clear-search').click(function () {
        $('#search-box').val('');
    });

})(window);
